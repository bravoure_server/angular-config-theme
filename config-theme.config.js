(function () {

    function themeConfig ($rootScope) {
        $rootScope.theme = 'theme-default';

        // Setting the favicon
        $rootScope.favicon = 'favicon-' + $rootScope.theme + '.png';

        $rootScope.$apply();
    }

    themeConfig.$inject = ['$rootScope'];

    angular
        .module('bravoureAngularApp')
        .run(themeConfig);

})();
